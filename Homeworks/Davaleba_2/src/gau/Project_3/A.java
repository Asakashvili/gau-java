package gau.Project_3;

import java.util.Scanner;

public class A {
    int x=2, y=3, z=4; // შემოვიტანე სამი 'მთელი' ცვლადი და მივანიჭე მნიშვნელობები პასუხების დასაბრუნებლად.

    // კლასი განვსაზღვრე x, y, z მეთოდით.
    public void method1() {
        Scanner sc = new Scanner(System.in);
        System.out.println("x = ");
        x = sc.nextInt();
        System.out.println("y = ");
        y = sc.nextInt();
        System.out.println("z = ");
        z = sc.nextInt();
    }

    // დააბრუნა 2x-7.
    public int method2() {
        return 2*x-7;
    }

    // დააბრუნა 3y+z.
    public int method3(){
        return 3*y-z;
    }

    // დააბრუნა 12y-x+z;
    public int method4(){
        return 12*y-x+z;
    }
}
