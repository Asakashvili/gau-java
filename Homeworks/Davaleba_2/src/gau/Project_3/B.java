package gau.Project_3;

import java.util.Scanner;

public class B {
    int n=2, y=1; // შემოვიტანე ორი 'მთელი' ცვლადი და მივანიჭე მნიშვნელობები პასუხების დასაბრუნებლად.

    public void method1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("n = ");
        n = sc.nextInt();
        System.out.println("y = ");
        y = sc.nextInt();
    }

    // დააბრუნა 2+1;
    public int method2(){
        return n+y;
    }

    // დააბრუნა 3-y;
    public int method3(){
        return 3-y;
    }
}
