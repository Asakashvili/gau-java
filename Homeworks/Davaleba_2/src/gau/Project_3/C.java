package gau.Project_3;

import java.util.Scanner;

public class C {
    double a=4.33, b=5.15; // შემოვიტანე ორი 'ნამდვილი' ცვლადი და მივანიჭე მნიშვნელობები პასუხების დასაბრუნებლად.

    public void method1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();
    }

    // დააბრუნა '1'.
    public double method2(){ // ვიყენებთ 'double'-ს თუ ცვლადებიც 'double'-თი შემოვიტანეთ.
        return 1+a*b;
    }

    // დააბრუნა '3-'.
    public double method3(){ // ვიყენებთ 'double'-ს თუ ცვლადებიც 'double'-თი შემოვიტანეთ.
        return 3-a;
    }
}
