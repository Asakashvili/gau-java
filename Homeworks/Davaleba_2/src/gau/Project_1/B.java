package gau.Project_1;

import java.util.Scanner;

public class B {
    private int a, b, c;

    // 'a', 'b' და 'c' ცვლადებს მივანიჭე მნიშვნელობები კლავიატურიდან.
    public void method1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();
        System.out.println("c = ");
        c = sc.nextInt();
    }
    // დავბეჭდე მოცემული სამ ცვლადს შორის მაქსიმალური.
    public void method2(){
        System.out.println(Math.max(Math.max(a, b), c)); // 'Math.max' ოპერატორი დაბეჭდავს მოცემული სამი ცვლადიდან მაქსიმალურ მნიშვნელობას.
    }
    // დავაბრუნე მოცემული სამი ცვლადიდან მინიმალური.
    public int method3(){
        return Math.min(Math.min(a, b), c); // 'Math.min' ოპერატორი დაგვიბრუნებს მოცემული სამი ცვლადიდან მინიმალურ მნიშვნელობას.
    }
}
