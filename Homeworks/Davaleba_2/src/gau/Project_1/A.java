package gau.Project_1;

import java.util.Scanner;

public class A {
    private int a, b; // გამოვიძახე კერძო ცვლადები, კერძო ცვლადები ხელმისაწვდომი იქნება მხოლოდ და მხოლოდ ამ კლასში.

    // 'a' და 'b' ცვლადებს მივანიჭე მნიშვნელობები კლავიატურიდან.
    public void method1(){ // 'void' - არ აბრუნებს რაიმე მნიშვნელობას კოდის დასრულებისას.
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();
    }
    // 'დავბეჭდე' 'a' და 'b' ცვლადების ჯამი.
    public void method2(){
        System.out.println("a+b="+(a+b));
    }
    // 'დავაბრუნე' 'a' და 'b' ცვლადების ნამრავლი. / დაბრუნებაზე როცაა საუბარი ვიყენებთ 'public int'.
    public int method3(){ // 'public int' 'return' ოპერატორის გამოსაყენებლად - 'Main' კლასში ეს კონრკეტული მეთოდი გამოგვაქვს 'System.out.println'-ით.
        return a*b;
    }
}

