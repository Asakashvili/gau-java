package gau.Project_1;

import java.util.Scanner;

public class C {
    int a, b, c;

    // 'a', 'b' და 'c' ცვლადებს მივანიჭე მნიშვნელობები კლავიატურიდან.
    public void method1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();
        System.out.println("c = ");
        c = sc.nextInt();
    }
    // დავაბრუნე 'a'-ს მნიშვნელობის ბოლო ციფრი.
    public int method2(){
        return a%10; // 10-ის მოდული.
    }
    // დავაბრუნე 'b'-ს მნიშვნელობის პირველი ციფრი.
    public int method3(){
        int x = 0; // დავწერე 'x' ცვლადი და მივანიჭე მნიშვნელობა '0'.
        while(b!=0){ // როცა 'b' არ უდრის '0'-ს.
            x = b%10;
            b = b/10;
        }
        return x; // ბოლოს 'while' ოპერატორის გარეთ ვაბრუნებთ 'x' ცვლადს.
    }

    // დავაბრუნე 'c' მნიშვნელობის ციფრთა ჯამი.
    public int method4(){
        int sum = 0;
        while(c!=0){
            sum += c%10;
            c = c/10;
        }
        return sum;
    }

    // დავაბრუნე 'method2' და 'method3'-ის ნამრალვი 'method5'-ში.
    public int method5(){
        return method2()*method3();
    }
    // დავაბრუნე 'method4' და 'method5'-ის ჯამი 'method6'-ში.
    public int method6(){
        return method4()+method5();
    }
}
