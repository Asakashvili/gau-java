package gau.Project_2;

import java.util.Random;
import java.util.Scanner;

public class B { // განვსაზღვრე კლასი 'B'.
    int[] m = new int[12]; // განვსაზღვრე მთელი ტიპის 'm[10]' მასივი.
    Random rand = new Random(); // შემოვიტანე რენდომი, ვინაიდან ამოცანაში ლაპარაკია შემთხვევით რიცხვებზე.

    // 'm' მასივის ელემენტებს მივანიჭე მნიშვნელობები კლავიატურიდან.
    public void method1() {
        Scanner sc = new Scanner(System.in);
        for (int i = 1; i < m.length; i++) {
            System.out.println("Number:" + i + ": ");
            m[i - 1] = sc.nextInt();
        }
    }

    // 'm' მასივს მივანიჭე შემთხვევითი რიცხვები [0;15] შუალედიდან.
    public void method2() {
        for (int i=0; i<m.length; i++) {
            m[i] = rand.nextInt(15);
            System.out.println(m[i]);
        }
    }

    // დავბეჭდე მასივის იმ ელემენტების მნშვნელობები შესაბამისი ინდექსებით, რომელთა მნიშვნელობებიც მეტია '7'-ზე.
    public void method3(){
        for (int i=0; i<m.length; i++)
            if (m[i]>7) {
                System.out.println(i+": "+m[i]);
            }
        }

    // გვიბრუნებს მასივის უდიდესი და უმცირესი ელემენტების სხვაობას. -
    public int method4(){
        int max=0, min=m[0];
        for (int j : m) {
            if (j>max) {
                max = j;
            }
            if (j<min) {
                min = j;
            }
        }
        System.out.println("Max: "+max+" Min: "+min);
        return max-min;
    }
}