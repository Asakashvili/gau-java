package gau.Project_2;

import java.util.Random;
import java.util.Scanner;

public class A { // განვსაზღვრე კლასი 'A'.
    int []m = new int[10]; // განვსაზღვრე მთელი ტიპის 'm[10]' მასივი.
    Random rand = new Random(); // შემოვიტანე რენდომი.

    // 'm' მასივის ელემენტებს მივანიჭე მნიშვნელობები კლავიატურიდან.
    public void method1(){
        Scanner sc = new Scanner(System.in);
        for(int i=1; i<=m.length; i++){
            System.out.println("Number"+i+": ");
            m[i-1] = sc.nextInt(); // '-1' დახმარებით დაინომრება შესატანი რიცხვები.
        }
    }

    // 'm' მასივის ელემენტებს მივანიჭე შემთხვევითი მნიშვნელობები [0;15] შუალედიდან.
    public void method2(){
        for(int i=0; i<10; i++){ //  '10' არის მოცემული მასივის ელემენტების რაოდენობა, ამ შემთხვევაში იგივეა რაც 'm.length'.
            m[i] = rand.nextInt(15);
        }
    }

    // დავაბრუნებინე იმ ელემენტების მნიშვნელობათა ჯამი რომლის მნიშვნელობა თავის ინდექსზე ნაკლებია.
    public int method3(){
        int sum=0;
        for(int i=0; i<m.length; i++){
            if(m[i] < i){
                sum+=m[i];
            }
        }
        return sum;
    }

    // დავაბრუნებინე იმ ელემენტების ინდექსების ნამრავლი, რომლის მნიშვნელობა თავის ინდექსზე მეტია.
    public int method4(){
        int x=1;
        for(int i=0; i<m.length; i++){
            if(m[i] > i){
                x*=m[i];
            }
        }
        return x;
    }
}


