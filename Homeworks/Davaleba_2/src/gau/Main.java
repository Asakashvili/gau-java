package gau;

import gau.Project_3.A;
import gau.Project_3.B;
import gau.Project_3.C;

public class Main {

    public static void main(String[] args) {
	// write your code here

    A a = new A();
    a.method1();
    a.method2(); // 'void' შემთხვევაში 'a.method-name()'.
    System.out.println(a.method3()); // 'int' შემთხვევაში 'System.out.println'.

//    B b = new B();
//    b.method1();
//    b.method2();
//    System.out.println(b.method3());
//    System.out.println(b.method4());

//    C c = new C();
//    c.method1();
//    System.out.println(c.method2());
//    System.out.println(c.method3());
//    System.out.println(c.method4());
//    System.out.println(c.method5());
//    System.out.println(c.method6());
    }
}
