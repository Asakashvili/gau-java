package gau.Practical;

import java.io.*;
import java.util.Scanner;

public class FilesAndFolders {
    int drive, select; // განვსაზღვრე 'drive'  და 'select' ცვლადები გლობალურად რომელიც ქვემოთ გამოვიყენე.
    // 'select' შიდა პასუხის არჩევისთვის რომ გარე და შიდა ერთმანეთში არ არეულიყვნენ.

    // შექმნის ძირითად "root" საქაღალდეს (რომელიმე დისკზე), დისკის ამორჩევა შეუძლია მომხმარებელს; Enter Drive Name:
    public void task1() { // განვსაზღვრე შიდა კლასი რომელსაც დავარქვი 'task1' პირველი ამოცანის განსახორციელებლად.
        Scanner sc = new Scanner(System.in); // გამოვიძახე სკანერი რომელიც წაიკითხავს კლავიატურიდან შეტანილ ბრძანებებს.
        System.out.println("Enter Drive Name (1 or 2) to create the root folder:\n1. C: Drive;\n2. E: Drive;"); // აქ დავბეჭდე კითხვა თავისი ვარიანტებით.
        drive = sc.nextInt(); // აქ პასუხის გაცემის შემდეგ ავტომატურად გადავედი ქვემოთ დაწერილ ოპერაციაზე.

        // გამოვიყენე მხოლოდ და მხოლოდ C: და E: დისკები ვინაიდან კომპიუტერში მხოლოდ მოცემული დისკები მაქვს.

        if (drive == 1) { // პირველი ქეისი, თუ მომხმარებელმა აირჩია 1 - (C დისკი).
            File file = new File("C:\\root"); // ამ შემთხვევაში 'C' დისკზე შეიქმნას 'root' ფოლდერი. (ფოლდერის სახელი განვსაზღვრე აქ).
            // აქ შევამოწმე არსებობს თუ არა ნახსენები ფოლდერი.
            if (!file.exists()) { // რა მოხდეს იმ შემთხვევაში თუ ფაილი/ფოლდერი არ არსებობს.
                file.mkdir(); // არ არსებობის შემთხვევაში შეიქმნება 'root' ფოლდერი.
                System.out.println("Congratulations! You have successfully created root on C: Drive!"); // ფოლდერის წარმატებთ შექმნის შემთხვევში გამოვა ტექსტი რომ შეიქმნა ფოლდერი კონკრეტულ დისკზე.
            } else if (file.exists()) { // იმ შემთხვევაში თუ ფოლდერი უკვე არსებობს.\
                System.out.println("The root folder already exists on C: Drive!"); // გამოვა ტექსტი რომ ამა და ამ დისკზე კონკრეტული ფოლდერი უკვე არსებობს.
            }
        }

        if (drive == 2) { // მეორე ქეისი, თუ მომხმარებელმა აირჩია 2 - (E დისკი).
            File file = new File("E:\\root"); // ამ შემთხვევაში 'E' დისკზე შეიქმნას 'root' ფოლდერი. (ფოლდერის სახელი განვსაზღვრე აქ).
            // აქ შევამოწმე არსებობს თუ არა ნახსენები ფოლდერი.
            if (!file.exists()) { // რა მოხდეს იმ შემთხვევაში თუ ფაილი/ფოლდერი არ არსებობს.
                file.mkdir(); // არ არსებობის შემთხვევაში შეიქმნება 'root' ფოლდერი.
                System.out.println("Congratulations! You have succesfully created root folder on E: Drive!"); // ფოლდერის წარმატებთ შექმნის შემთხვევში გამოვა ტექსტი რომ შეიქმნა ფოლდერი კონკრეტულ დისკზე.
            } else if (file.exists()) { // იმ შემთხვევაში თუ ფოლდერი უკვე არსებობს.
                System.out.println("The root folder already exists on E: Drive!"); // გამოვა ტექსტი რომ ამა და ამ დისკზე კონკრეტული ფოლდერი უკვე არსებობს.
            }
        }

        if (drive != 1 && drive != 2) { // აქ უკვე დავწერე ერორის შემთხვევა, რა მოხდეს თუ მომხმარებელი აირჩევს ციფრებს გარდა ერთი ან ორისა. (თუ 'drive' ცვლადი არ უდრის ერთს ან ორს).
            System.out.println("Error choosing the path! Please do insert either number 1 or number 2!"); // გამოვა შემდეგი პარაგრაფი სადაც სისტემა მომხმარებელს მოსთხოვს მხოლოდ 1 ან 2 ციფრების შეყვანას.
        }
    }

    // ძირითად საქაღალდეში ქმნის საქაღალდეებს ან ფაილებს (ირჩევს მომხმარებელი);
    public void task2() {
        Scanner sc = new Scanner(System.in); // გამოვიძახე სკანერი.
        // აქ მომხმარებელს მივეცი საშუალება რომ აერჩია კონკრეტულ დისკზე მყოფი ფოლდერი ამ ფოლდერში ახალი ფოლდერის ან ფაილის შესაქმნელად.
        System.out.println("\nPlease select a dictionary path on specific drive to create either a Folder or a File:\n1. C: Drive\\root;\n2. E: Drive\\root;");
        drive = sc.nextInt(); // აქ გავაკეთე გადასვლა შემდეგ ოპერაციაზე პასუხის გაცემის შემდგომ.

        // რა მოხდეს იმ შემთხვევაში, თუ მომხმარებელი აირჩევს 1.-ს ანუ 'C' დისკს.
        if (drive == 1) {
            File file = new File("C:\\root"); // აქ მივუთითე 'C' დისკზე მყოფი ფოლდერის ლოკაცია იმის გადასამოწმებლად არსებობს თუ არა იცი ამ დისკზე.
            // აქ შევამოწმე არსებობს თუ არა ნახსენები ფოლდერი.
            if (!file.exists()) { // თუ ეს ფოლდერი არ არსებობს.
                System.out.println("Error! The root folder on C: Drive does NOT exist!"); // თუ ეს ფოლდერი არ არსებობს გამოვა ნაჩვენები ერორი.
            } else if (file.exists()) { // იმ შემთხვევაში თუ ეს ფოლდერი უკვე არსებობს.
                // ამ ფოლდერის არსებობის შემთხვევაში მომხარებელს საშუალება მიეცემა წარმატებით აირჩიოს ეს ლოკაცია და გადავიდეს შემდეგ ოპერაციაზე.
                System.out.println("You successfully selected the root folder on C: Drive!\n\nWhat do you want to create in selected root folder?\n1. A Folder;\n2. A File;");
                // '\n'-ის მეშვეობით ზემოთვე გამოვყავი აბზაცი და მომხმარებელს საშუალება მივეცი რომ გადავიდეს შემდეგ ოპერაციაზე შესაქმნელი ფოლდერის ან ფაილის არჩევის შემდგომ.
                select = sc.nextInt(); // გადავედი შემდეგ ოპერაციაზე - 'drive' ჩავანაცვლე 'select'-ით რომ 'select'-ს ემუშავა!

                // რა მოხდეს 1.-ის ანუ ფოლდერის შექმნის არჩევის შემთხვევაში.
                if (select == 1) {
                    File myObj = new File("C:\\root\\New Folder"); // აქ დავწერე ლოკაცია თუ სად შეიქმნას ფოლდერი და რა ერქვას.
                    // აქ შევამოწმე არსებობს თუ არა ნახსენები ფოლდერი.
                    if (!myObj.exists()) { // თუ ფოლდერი არ არსებობს.
                        myObj.mkdir(); // ამის მეშვეობით შევქმნათ ფოლდერი ზემოთ გაწერილ ლოკაციაზე შესაბამისი სახელით.
                        // გამოვიდეს ტექსტი რომ შეიქმნა ფოლდერი 'C' დისკზე.
                        System.out.println("Congratulations! You have succesfully created a new Folder inside the root folder located on E: Drive!");
                    } else if (myObj.exists()) { // იმ შემთხვევაში თუ ფოლდერი უკვე არსებობს.
                        System.out.println("Folder already exists!"); // ფოლდერი ვეღარ შეიქმნება და გამოვა შესაბამისი ტექსტი რომ ის უკვე არსებობს.
                    }
                }

                // რა მოხდეს 2.-ის ანუ ფაილის შექმნის არჩევის შემთხვევაში.
                if (select == 2) {
                    try { // აქ უკვე შემოვიტანე 'try', ანუ ცდა.
                        File myObj = new File("C:\\root\\file"); // აქ დავწერე ლოკაცია თუ სად შეიქმნას ფაილი და რა ერქვას.
                        if (myObj.createNewFile()) { // აქედან შევქმენი ეს ფაილი.
                            // აქ უკვე გამოვიდა ტექსტი რომ შეიქმნა ესა და ეს ფაილი. (ქვემოთ პლიუსებში გადმოვიტანე სახელი).
                            System.out.println("Congratulations! You have succesfully created a " + myObj.getName() + " inside the root folder located on C: Drive!");
                        } else { // წინააღმდეგ შემთხვევაში (თუ ფაილი უკვე არსებობს).
                            System.out.println("File already exists!"); // გამოვა ტექსტი რომ ფაილი უკვე არსებობს.
                        }
                    } catch (IOException e) { // აქ თუ რაიმე სწორად ვერ ჩაიტვირთა შემოწმდება და გამოვა ქვემოთ მოცემული ტექსტი.
                        System.out.println("An error occurred.");
                        e.printStackTrace();
                    }
                }
            }
        }

        // რა მოხდეს იმ შემთხვევაში, თუ მომხმარებელი აირჩევს 2.-ს ანუ 'E' დისკს.
        if (drive == 2) {
            File file = new File("E:\\root"); // აქ მივუთითე 'E' დისკზე მყოფი ფოლდერის ლოკაცია იმის გადასამოწმებლად არსებობს თუ არა იცი ამ დისკზე.
            // აქ შევამოწმე არსებობს თუ არა ნახსენები ფოლდერი.
            if (!file.exists()) { // თუ ეს ფოლდერი არ არსებობს.
                System.out.println("Error! The root folder on C: Drive does NOT exist!"); // თუ ეს ფოლდერი არ არსებობს გამოვა ნაჩვენები ერორი.
            } else if (file.exists()) { // იმ შემთხვევაში თუ ფოლდერი უკვე არსებობს.
                // ამ ფოლდერის არსებობის შემთხვევაში მომხარებელს საშუალება მიეცემა წარმატებით აირჩიოს ეს ლოკაცია და გადავიდეს შემდეგ ოპერაციაზე.
                System.out.println("You successfully selected the root folder on E: Drive!\n\nWhat do you want to create in selected root folder?\n1. A Folder;\n2. A File;");
                // '\n'-ის მეშვეობით ზემოთვე გამოვყავი აბზაცი და მომხმარებელს საშუალება მივეცი რომ გადავიდეს შემდეგ ოპერაციაზე შესაქმნელი ფოლდერის ან ფაილის არჩევის შემდგომ.
                select = sc.nextInt(); // გადავედი შემდეგ ოპერაციაზე - 'drive' ჩავანაცვლე 'select'-ით რომ 'select'-ს ემუშავა!

                // რა მოხდეს 1.-ის ანუ ფოლდერის შექმნის არჩევის შემთხვევაში.
                if (select == 1) {
                    File myObj = new File("E:\\root\\New Folder"); // აქ დავწერე ლოკაცია თუ სად შეიქმნას ფოლდერი და რა ერქვას.
                    // აქ შევამოწმე არსებობს თუ არა ნახსენები ფოლდერი.
                    if (!myObj.exists()) { // თუ ფოლდერი არ არსებობს.
                        myObj.mkdir(); // ამის მეშვეობით შევქმნათ ფოლდერი ზემოთ გაწერილ ლოკაციაზე შესაბამისი სახელით.
                        // გამოვიდეს ტექსტი რომ შეიქმნა ფოლდერი 'E' დისკზე.
                        System.out.println("Congratulations! You have succesfully created a new Folder inside the root folder located on E: Drive!");
                    } else if (myObj.exists()) { // იმ შემთხვევაში თუ ფოლდერი უკვე არსებობს.
                        System.out.println("Folder already exists!"); // ფოლდერი ვეღარ შეიქმნება და გამოვა შესაბამისი ტექსტი რომ ის უკვე არსებობს.
                    }
                }


                // რა მოხდეს 2.-ის ანუ ფაილის შექმნის არჩევის შემთხვევაში.
                if (select == 2) {
                    try { // აქ უკვე შემოვიტანე 'try', ანუ ცდა.
                        File myObj = new File("E:\\root\\file"); // აქ დავწერე ლოკაცია თუ სად შეიქმნას ფაილი და რა ერქვას.
                        if (myObj.createNewFile()) { // აქედან შევქმენი ეს ფაილი.
                            // აქ უკვე გამოვიდა ტექსტი რომ შეიქმნა ესა და ეს ფაილი. (ქვემოთ პლიუსებში გადმოვიტანე სახელი).
                            System.out.println("Congratulations! You have succesfully created a " + myObj.getName() + " inside the root folder located on E: Drive!");
                        } else { // წინააღმდეგ შემთხვევაში (თუ ფაილი უკვე არსებობს).
                            System.out.println("File already exists!"); // გამოვა ტექსტი რომ ფაილი უკვე არსებობს.
                        }
                    } catch (IOException e) { // აქ თუ რაიმე სწორად ვერ ჩაიტვირთა შემოწმდება და გამოვა ქვემოთ მოცემული ტექსტი.
                        System.out.println("An error occurred.");
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // ჩაწერს ნებისმიერ ფაილში მონაცემებს რომლებიც შეაქვს მომხმარებელს.
    public void task3() {
        Scanner sc = new Scanner(System.in); // გამოვიძახე სკანერი.
        // რა მოხდეს 1.-ის ანუ ფოლდერის შექმნის არჩევის შემთხვევაში.
        if (drive == 1){ //
            System.out.println("\nPlease write a text in the file: "); // დავბეჭდე მოცემული ტექსტი.
            String text = ""; // გამოვიყენე სტრიქონი და დავარქვი 'text', რომელიც გავუტოლე უსასრულობას ვინაიდან მას მომხმარებელი შეავსებს კოდის გაშვების შემდეგ.
            text = sc.next(); // ამით გადავედი შემდეგ ოპერაციაზე.

            try { // ცდა.
                FileWriter myWriter = new FileWriter("C:\\root\\file", true); // გამოვიძახე 'FileWriter' და მივუთითე ლოკაცია თუ სად უნდა ჩაწეროს მომხმარებელმა რაიმე.
                // ასევე, 'true'-ს გამოყენებით მომხმარებლის მიერ ჩაწერილი ჩანაწერები ძველებს ემატება, 'false'-ს შემთხვევაში ჩაანაცვლებდა.
                myWriter.write(text + "\n"); // აქ ჩაწერას გადავეცი ზემოთ მოცემული სიცარიელის მქონე ცვლადი, ანუ დაიბეჭდება ის რასაც მომხმარებელი ჩაწერს.
                // "\n"-ის ჩამატებით ტექსტის ჩაწერის თითოეულ ჯერზე ახალი სტრიქონი გამოდის აბზაცით.
                myWriter.close(); // აქ დავხურე ჩაწერა, ანუ დავასრულე.
                System.out.println("Congratulations! You have successfully wrote in the file!"); // აქ წარმატებით ჩაწერის შემთხვევაში გამოვა ასეთი შეტყობინება.
            } catch (IOException e) { // აქ თუ რაიმე სწორად ვერ ჩაიტვირთა შემოწმდება და გამოვა ქვემოთ მოცემული ტექსტი.
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }

        // რა მოხდეს 2.-ის ანუ ფოლდერის შექმნის არჩევის შემთხვევაში.
        if (drive == 2){
            System.out.println("\nPlease write a text in the file: "); // დავბეჭდე მოცემული ტექსტი.
            String text = ""; // გამოვიყენე სტრიქონი და დავარქვი 'text', რომელიც გავუტოლე უსასრულობას ვინაიდან მას მომხმარებელი შეავსებს კოდის გაშვების შემდეგ.
            text = sc.next(); // ამით გადავედი შემდეგ ოპერაციაზე.

            try { // ცდა.
                FileWriter myWriter = new FileWriter("E:\\root\\file", true); // გამოვიძახე 'FileWriter' და მივუთითე ლოკაცია თუ სად უნდა ჩაწეროს მომხმარებელმა რაიმე.
                // ასევე, 'true'-ს გამოყენებით მომხმარებლის მიერ ჩაწერილი ჩანაწერები ძველებს ემატება, 'false'-ს შემთხვევაში ჩაანაცვლებდა.
                myWriter.write(text + "\n"); // აქ ჩაწერას გადავეცი ზემოთ მოცემული სიცარიელის მქონე ცვლადი, ანუ დაიბეჭდება ის რასაც მომხმარებელი ჩაწერს.
                // "\n"-ის ჩამატებით ტექსტის ჩაწერის თითოეულ ჯერზე ახალი სტრიქონი გამოდის აბზაცით.
                myWriter.close(); // აქ დავხურე ჩაწერა, ანუ დავასრულე.
                System.out.println("Congratulations! You have successfully wrote in the file!"); // აქ წარმატებით ჩაწერის შემთხვევაში გამოვა ასეთი შეტყობინება.
            } catch (IOException e) { // აქ თუ რაიმე სწორად ვერ ჩაიტვირთა შემოწმდება და გამოვა ქვემოთ მოცემული ტექსტი.
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }

    // წაიკითხავს ფაილებს და დაბეჭდავს შიგნით არსებულ მოანცემებს /მომხმარებლის სურვილის შესაბამისად/.
    public void task4(){
        System.out.println("\nFile contents: ");

        // რა მოხდეს იმ შემთხვევაში, თუ მომხმარებელი აირჩევს 1.-ს ანუ 'C' დისკს.
        if(drive == 1){
            try (BufferedReader br = new BufferedReader(new FileReader("C:\\root\\file"))) { // აქ შევქმენი ახალი 'FileReader' რის მეშვეობითაც ვხსნით და ვკითხულობთ ფაილს.
                String line = null; // აქ გამოვიძახე სტრიქონი და დავარქვი სახელი 'line' და გავუტოლე 'null'-ს.
                while ((line = br.readLine()) != null) { // როცა 'line' ცვლადი არ უდრის ზემოთ მოცემულ 'br' ცვლადს.
                    System.out.println(line); // მაშინ დაბეჭდოს ფაილის შიგთავსი.
                }
            } catch (IOException e) { // აქ თუ რაიმე სწორად ვერ ჩაიტვირთა შემოწმდება და გამოვა ქვემოთ მოცემული ტექსტი.
                e.printStackTrace();
            }
        }

        // რა მოხდეს იმ შემთხვევაში, თუ მომხმარებელი აირჩევს 2.-ს ანუ 'E' დისკს.
        if (drive == 2){
            try (BufferedReader br = new BufferedReader(new FileReader("E:\\root\\file"))) { // აქ შევქმენი ახალი 'FileReader' რის მეშვეობითაც ვხსნით და ვკითხულობთ ფაილს.
                String line = null; // აქ გამოვიძახე სტრიქონი და დავარქვი სახელი 'line' და გავუტოლე 'null'-ს.
                while ((line = br.readLine()) != null) { // როცა 'line' ცვლადი არ უდრის ზემოთ მოცემულ 'br' ცვლადს.
                    System.out.println(line); // მაშინ დაბეჭდოს ფაილის შიგთავსი.
                }
            } catch (IOException e) { // აქ თუ რაიმე სწორად ვერ ჩაიტვირთა შემოწმდება და გამოვა ქვემოთ მოცემული ტექსტი.
                e.printStackTrace();
            }
        }
    }
}

