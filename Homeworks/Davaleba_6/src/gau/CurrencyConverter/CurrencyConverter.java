package gau.CurrencyConverter;

import java.text.DecimalFormat; // შემოტანილია ათობითი ფორმატი.
import java.util.Scanner; // შემოტანილია სკანერი.

public class CurrencyConverter implements CurrencyConverterInterface { // შევქმენი კლასი რომელსაც დავარქვი 'CurrencyConverter' და მივაბი ინტერფეისი.
    // გლობალურად, შიდა კლასების გარეთ დავწერე ცვლადები.
    int code; // ეს ცვლადი დავწერე 'sc.nextInt()'-სთვის და ნუმერაციის 'if' ოპერატორში წარმოსადგენად.
    double amount, usdcurrency, gbpcurrency, eurcurrency; // აქ დავწერე 'double' ოპერატორი და განვსაზღვრე ცვლადები ნამდვილი რიცხვებისთვის.
    double USD = 1.7710, GBP = 2.8950, EUR = 2.2700; // აქაც ანალოგიურად დავწერე მცურავწერტილიანი, ნამდვილი რიცხვებისთვის 'double' ოპერატორი და მასში შეტანილ ცვლადებს მივანიჭე მნიშვნელობები ისე, როგორც ამოცანში იყო მოცემული.
    DecimalFormat f = new DecimalFormat("##.##"); // შემოვიტანე ათობითი ფორმატი რომელიც ქვემოთ თანხის ოდენობის გამოსატანმად გამოვიყენე.

    // დავწერე შიდა კლასი.
    public void CurrencyConverter(){
        Scanner sc = new Scanner(System.in); // შემოვიტანე სკანერი.

        System.out.println("აირჩიეთ რომელი ვალუტის კონვერტაცია გსურთ ლარში: "); // დავბეჭდე მოცემული წინადედბა.

        System.out.println("\n1. United States Dollar (USD);\n2. British Pound Sterling (GBP);\n3. Euro (EUR);\n"); // დავბეჭდე მოცემული წინადედბა & '\n'-ის მეშვეობით გამოვყავი აბზაცები.
        code = sc.nextInt(); // გამოვიყენე ზემოთ ნახსენები ცვლადი რისი მეშვეობითაც კონკრეტული ვალუტის არჩევის შემდეგ ავტომატურად გადავალთ ქვემოთ მოცემულ შემდეგ კითხვაზე.

        System.out.println("შეიყვანეთ თანხის ოდენობა არჩეული ვალუტის ლარში კონვერტაციისთვის: "); // დავბეჭდე მოცემული წინადადება რომელიც პირველ კითხვაზე გაცემული პასუხის შემდგომ გამოდის.
        amount = sc.nextFloat(); // ანალოგიურად, პასუხის შეყვანის შემდეგ 'sc.nextFloat'-ით (მცურავწერტილიანი) გადავედი შემდეგ ოპერაციებზე.

        // აქ უკვე გამოვიყენე 'if' ოპერატორები.
        if (code == 1){ // თუ 'code' უდრის ერთს ანუ 'USD'-ს, მაშინ:
            usdcurrency = amount / USD; // 'usdcurrency' ცვლადი გავუტომე 'amount' ცვლადს და გავყავი 'USD'-ზე რომლის მნიშვნელობაც ზემოთ განვსაზღვრე.
            System.out.println(amount + " დოლარი ლარებში შეადგენს: " + f.format(USD) + " ლარს."); // აქ დავბეჭდე კონკრეტული დოლარის ოდენობა ლარებში.
        }

        if (code == 2){ // თუ 'code' უდრის ორს ანუ 'GBP'-ს, მაშინ:
            gbpcurrency = amount / GBP; // 'gbpcurrency' ცვლადი გავუტომე 'amount' ცვლადს და გავყავი 'GBP'-ზე რომლის მნიშვნელობაც ზემოთ განვსაზღვრე.
            System.out.println(amount + " გირვანქა სტერლინგი ლარებში შეადგენს: " + f.format(GBP) + " ლარს."); // აქ დავბეჭდე შეტანილი გირვანქა სტერლინგის ოდენობა ლარებში.
        }

        if (code == 3){ // თუ 'code' უდრის ორს ანუ 'GBP'-ს, მაშინ:
            eurcurrency = amount / EUR; // 'eurcurrency' ცვლადი გავუტომე 'amount' ცვლადს და გავყავი 'EUR'-ზე რომლის მნიშვნელობაც ზემოთ განვსაზღვრე.
            System.out.println(amount + " ევრო ლარებში შეადგენს: " + f.format(EUR) + " ლარს."); // აქ დავბეჭდე შეტანილი ევროს ოდენობა ლარებში.
        }
    }
}
