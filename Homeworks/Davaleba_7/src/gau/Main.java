package gau;

import gau.Davaleba_7.Tasks_7;
import gau.Midterm_Exercises.Task_1.A;
import gau.Midterm_Exercises.Task_2.FamilyBudget;
import gau.Midterm_Exercises.Task_2.FamilyMember;

public class Main {

    public static void main(String[] args) {
	// write your code here

        // Midterm_Examples;
//        A a = new A();
//        a.method1();
//        System.out.println(a.method2()); // დაბრუნებისთვის გამოვიყენე 'return', რაც 'Main' კლასში 'SOUT'-ში იწერება.
//        System.out.println(a.method3()); // დაბრუნებისთვის გამოვიყენე 'return', რაც 'Main' კლასში 'SOUT'-ში იწერება.
//        a.method4();

//        FamilyBudget fambudget = new FamilyBudget();
//        fambudget.setMoney(13000); // აქ გადავეცი ფულის ოდენობა.
//        FamilyMember fm = new FamilyMember("Giorgi", "Asakashvili", 22, "Student"); // აქ გადავეცი შესაბამისი ცვლადის პარამეტრები.

        // Davaleba_7;
        Tasks_7 t7 = new Tasks_7();
//        t7.Task_1(121); // კონკრეტულ ამოცანას გადავეცი რიცხვი.
        t7.Task_2();
//        t7.Task_4();
//        t7.Task_5();

    }
}
