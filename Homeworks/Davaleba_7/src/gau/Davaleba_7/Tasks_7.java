package gau.Davaleba_7;

import java.util.Arrays;
import java.util.Scanner;

public class Tasks_7 {
    int a, b, c; // განვსაზღვრე ცვლადები 'Task_2'-სთვის.

    public void Task_1(Integer num) {
        String str = num.toString();
        StringBuilder newStr = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            newStr.append(str.charAt(i));
        }
        System.out.println("თავდაპირველი: " + str);
        System.out.println("შებრუნებული: " + newStr);
        if (str.equals(newStr.toString())) {
            System.out.println("რიცხვი პოლიდრომია");
        } else {
            System.out.println("რიცხვი არ არის პოლიდრომი");
        }
    }

    public void Task_2(){
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();
        System.out.println("c = ");
        c = sc.nextInt();

        if(a != b && a != c && b != c){
            System.out.println("NO");
        }else if(a == b){
            System.out.println("1. " +a);
            System.out.println("2. " +b);
        }else if(a == c){
            System.out.println("1. " +a);
            System.out.println("3. " +c);
        }else if(b == c){
            System.out.println("2. " +b);
            System.out.println("3. " +c);
        }
    }

    public void Task_4(){
        int[][] arr = new int[6][5];
        for(int i=0; i<=arr.length-1; i++){
            for(int j=0; j<=arr[0].length-1; j++){
                arr[i][j] = (int)(Math.random()*101);
            }
        }
        System.out.println("Before");
        for(int i=0; i<=arr.length-1; i++){
            for(int j=0; j<=arr[0].length-1; j++){
                if(j!=arr[0].length-1){
                    System.out.print(arr[i][j] + ", ");
                }
                else{
                    System.out.println(arr[i][j]);
                }
            }
        }

        for(int i=0; i<=arr.length-1; i++){
            for(int j=0; j<=arr.length-1; j++){
                int tmp = 0;
                if (arr[i][0] > arr[j][0])
                {
                    for(int k=0; k<=arr[0].length-1; k++) {
                        tmp = arr[i][k];
                        arr[i][k] = arr[j][k];
                        arr[j][k] = tmp;
                    }
                }
            }
        }
        System.out.println("After");

        for(int i=0; i<=arr.length-1; i++){
            for(int j=0; j<=arr[0].length-1; j++){
                if(j!=arr[0].length-1){
                    System.out.print(arr[i][j] + ", ");
                }
                else{
                    System.out.println(arr[i][j]);
                }
            }
        }
    }

    public void Task_5(){
        int[] arr = new int[100];
        int count=0;
        for (int i=0; i<arr.length; i++) {
            arr[i] = (int)(Math.random()*100);
            if(arr[i] > i){count++;}
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("მასივში არის " + count + " შემავალი ელემენტი რომელიც მისივე ინდექსზე მეტია.");
    }
}
