package gau.Midterm_Exercises.Task_2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class FamilyBudget { // განვსაზღვრე კლასი 'FamilyBudget'.
    private int money; // განვსაზღვრე მთელი ტიპის(int) დახურული(private) 'money' ცვლადი.
    String addr = "src/gau/Midterm_Exercises/Task_2/FamilyData"; // აქ მივუთითე ლოკაცია.

    public void setMoney(int money) {
        this.money = money;
        saveBudgetState();
    }

    public String FamilyState(){ // დავწერე მეთოდი, რომელიც დაადგენს 'FamilyBudget' მდგომარეობას 'money' თვისების მიხედვით.
        if(money>40000){ // თუ 'money' ცვლადი მეტია 40,000-ზე.
            return "Rich"; // ამ შემთხვევაში დააბრუნოს სიტყვა "მდიდარი".
        }else if(money>10000){ // თუ 'money' ცვლადი მეტია 10,000-ზე.
            return "Average"; // ამ შემთხვევაში დააბრუნოს "საშუალო".
        }
        else{ // წინააღმდეგ შემთხვევაში.
            return "Poor"; // დააბრუნოს "ღარიბი".
        }
    }

    public void saveBudgetState(){
        createFolder();
        try {
            FileWriter write = new FileWriter(addr+"/budget.txt", true); // აქ დავწერე თუ რა ფაილი შეიქმნას კონკრეტულ ლოკაციაზე.
            Date date = new Date();
            write.write(date.toString() + "--> " + FamilyState());
            write.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // აქედან შევქმენი ფოლდერი.
    public void createFolder(){
        File directory = new File(addr);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }
}
