package gau.Midterm_Exercises.Task_2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

    public class FamilyMember { // განვსაზღვრე "FamilyMember" კლასი.
        private String name, lastName, status; // განვსაზღვრე დახურული ცვლადები.
        private int age;
        protected String addr = "src/gau/Midterm_Exercises/Task_2/FamilyData"; // მივუთითე ლოკაცია თუ სად შეიქმნას კონკრეტული ფაილი.


        public FamilyMember(String name, String lastName, int age, String status) {
            this.name = name;
            this.lastName = lastName;
            this.age = age;
            this.status = status;
            saveFamily();
        }

        public void saveFamily() {
            creteFolder();
            try {
                FileWriter write = new FileWriter(addr + "/family.txt");
                Date date = new Date();
                write.write("Name: " + name + "\nLastName: " + lastName + "\nAge: " + age + "\nStatus: " + status); // აქედან 'family.txt' ფაილში ჩავწერე მოცემული პარამეტრები.
                write.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // აქედან შევქმენი ფოლდერი.
        public void creteFolder(){
            File directory = new File(addr);
            if (!directory.exists()) {
                directory.mkdir();
            }
        }
    }

