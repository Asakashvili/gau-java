package gau.Midterm_Exercises.Task_1;


import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

// შემოვიტანე ინტერფეისი, რომელსაც ჩავუწერე პირველი მეთოდი. (მეთოდი საფუნქციონიროდ ჩავწერე 'Main' კლასშიც).
interface InterfaceForA{
    void method1();
}

// განვსაზღვრე კლასი 'A' და მივაბი 'interface' რომელიც ზემოთ დავწერე.
public class A implements InterfaceForA {
    int a, b;
    int[] m = new int[15]; // განვსაზღვრე მთელი ტიპის 'm[15]' მასივი.

    // მეთოდი 1. ანიჭებს 'm' მასივის ელემენტებს შემთხვევით მნიშვნელობებს [a; b] შუალედიდან.
    // 'a' და 'b'-ს მნიშვნელობების შეტანა ხდება კლავიატურიდან.
    public void method1() {
        // 'a' და 'b'-ს მნიშვნელობები შემოვიტანე კლავიატურიდან.
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();

        // მივანიჭე 'm' მასივის ელემენტებს შემთხვევით მნიშვნელობებს [a; b] შუალედიდან.
        Random rand = new Random();
        for (int i = 0; i < m.length; i++) {
            m[i] = rand.nextInt(a, b);
        }
        System.out.println(Arrays.toString(m));
    }

    // აბრუნებს მასივის იმ ელემენტების მნიშვნელობათა ჯამს, რომლის მნიშვნელობა თავის ინდექსზე ნაკლებია.
    public int method2(){ // რადგანაც საუბარი იყო დაბრუნებაზე, ანუ 'return'-ის გამოყენებაზე, განვსაზღვრე 'int' ტიპის კლასი.
        int sum = 0;
        for (int i=0; i < m.length; i++) {
            if(m[i] < i)
                sum += m[i];
        }
        return sum; // დაბრუნებისთვის გამოვიყენე 'return', რაც 'Main' კლასში 'SOUT'-ში იწერება.
    }

    // აბრუნებს მასივის იმ ელემენტების ინდექსების ნამრავლს, რომლის მნიშვნელობა თავის ინდექსზე მეტია.
    public int method3(){ // რადგანაც საუბარი იყო დაბრუნებაზე, ანუ 'return'-ის გამოყენებაზე, განვსაზღვრე 'int' ტიპის კლასი.
        int mp = 1;
        for (int i=0; i < m.length; i++){
            if(m[i] < i)
                mp *= m[i];
        }
        return mp; // დაბრუნებისთვის გამოვიყენე 'return', რაც 'Main' კლასში 'SOUT'-ში იწერება.
    }

    // ბეჭდავს მასივში არსებულ ლუწ რიცხვებს.
    public void method4() {
        System.out.println("* EVEN NUMBERS:");
        for (int i = 0; i < m.length; i++) {
            if (m[i] % 2 == 0) {
                System.out.println(m[i]);
            }
        }
    }
}
