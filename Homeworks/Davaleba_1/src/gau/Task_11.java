package gau;

import java.util.Random;

public class Task_11 {
    public Task_11(){
        int arr[] = new int[8]; // რვა ელემენტიანი ნამდვილ რიცხვთა მასივი.
        Random rand = new Random(); // შემოვიტანე რენდომ ოპერატორი რადგან ამოცანაში საუბარი იყო შემთხვევით რიცხვებზე.

        for(int i=0; i<8; i++){
            arr[i] = rand.nextInt(25);
        }
        for(int i=0; i<8; i++){
            System.out.println(i+1+": "+arr[i]); // 'i+1'-ით დავნომრე, 'arr[i]'-ს მეშვეობით კი რენდომად გამოვიტანე მასივში შემავალი რიცხვები.
        }
    }
}
