package gau;

import java.util.Scanner;

public class Task_5 {
    public Task_5() {
        // შემოვიტანე ოთხი ცვლადი რადგან ოთხნიშნა რიცხვზეა საუბარი.
        int x, y, z = 0, count = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("შეიყვანეთ რაიმე ოთხნიშნა მთელი რიცხვი: ");
        x = sc.nextInt();
        y = x;

        while (y!=0) {
            y = y/10;
            count++;
        }

        if (count == 4) { // 'count' ცვლადი გავუტოლე 4-ს რადგან ოთხნიშნა რიცხვზეა ლაპარაკი.
            while (x!=0) {
                z += x%10; // '+=' - დამატებითი მნიშვნელობის მინიჭება.
                x = x/10;
            }
            System.out.println("თქვენს მიერ შეყვანილი ციფრების ჯამია: " +z);
        }else{
            System.out.println("შეიყვანეთ ოთხნიშნა მთელი რიცხვი!");
        }
    }
}
