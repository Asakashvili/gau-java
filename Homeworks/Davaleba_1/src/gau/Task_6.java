package gau;

import java.util.Scanner;

public class Task_6 {
    public Task_6(){
        int x, y=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("შეიყვანეთ რაიმე რიცხვი მასში შემავალი ციფრების ჯამის გამოსატანად: ");
        x = sc.nextInt();
        while(x!=0){ // თუ 'x' ცვლადი არ უდრის 0-ს.
            y += x%10; // '+=' - დამატებითი მნიშვნელობის მინიჭება.
            x = x/10;
        }
        System.out.println(y);
    }
}
