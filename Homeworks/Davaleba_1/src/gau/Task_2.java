package gau;

import java.util.Scanner;

public class Task_2 {
    public Task_2(){
        int x, y; // შემოვიტანე ორი ცვლადი.
        Scanner sc = new Scanner(System.in); // სკანერის დახმარებით ქვემოთ 'x' დაწერილი გამოვა კითხვის სახით.
        System.out.println("x = ");
        x = sc.nextInt(); // პასუხის ჩაწერის შემდეგ გადავა შემდეგ კითხვაზე.
        System.out.print("y = ");
        y = sc.nextInt(); // ბოლოს უკვე გამოიტანს მიღებულ შედეგებს.

        System.out.println("მთელი შედეგი: "+(x/y)); // ჯერ გამოიტანს პირველი რიცხვის მეორეზე გაყოფისას მიღებულ მთელ შედეგს.
        System.out.println("ნაშთი: "+(x%y)); // და მეორის პირველზე გაყოფისას მიღებულ ნაშთს.
    }
}
