package gau;

import java.util.Arrays;

public class Task_10 {
    public Task_10(){
        double arr[] = {5.43, 3.33, 2.32, 6.47, 1.22, 8.67, 4.45, 8.99}; // განვსაზღვრე 8 ელემენტიანი ნამდვილ რიცხვთა მასივი.
        Arrays.sort(arr);

        double x;
        for(int i=0; i>arr.length/2; i++){ // მასივის სიგრძე გავყავი 2-ზე. // აქ ასევე წერია თუ რა მიმდევრობით დავბეჭდე მასივი, ზრდადობით თუ კლებადობით. (ამ შემთხვევაში ზრდადობით.).
            x = arr[i];
            arr[i] = arr[arr.length-1-i];
            arr[arr.length-1-i] = x;
        }
        for(int i=0; i<arr.length; i++){
            System.out.println((i+1)+" - "+arr[i]);
        }
    }
}
