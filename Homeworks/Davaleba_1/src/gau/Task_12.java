package gau;

import java.util.Arrays;
import java.util.Random;

public class Task_12 {
    public Task_12(){
        int arr[] = new int[8];
        Random rand = new Random();
        for(int i=0; i<arr.length; i++){
            arr[i] = rand.nextInt(30);
        }
        Arrays.sort(arr);
        for(int i=0; i<arr.length/2; i++){ // შემთხვევითი რიცხვები 8-დან 30-მდე კლებადობით.
            int x = arr[i];
            arr[i] = arr[arr.length-1-i];
            arr[arr.length-1-i] = x;
        }
        for(int i=0; i<arr.length; i++){
            System.out.println((i+1)+" - "+arr[i]);
        }
    }
}
