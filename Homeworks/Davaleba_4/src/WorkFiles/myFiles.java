package WorkFiles;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class myFiles {
    // შემოვიტანე ცვლადი და მნიშვნელობად მივანიჭე კონკრეტული ლოკაცია ახალი ფოლდერის შესაქმნელად.
    protected String location = "src/myFiles"; // აქვე განვსაზღვრე საქაღალდის სახელი.
    // 'protected'-ის მეშვეობით ეს ცვლადი ხელმისაწვდომი იქნება მხოლოდ ამ კლასში.

    // ამ ფუნქციის საშუალებით შევქმენი ახალი ფოლდერი.
    public boolean cFolder() { // დავწერე ფუნქცია რომელსაც დავარქვი 'cFolder'.
        File f = new File(location); // გამოვიძახე 'File' ოპერატორი, ცვლადს დავარქვი 'f' და გავუტოლე შესაბამის მნიშვნელობას რასაც 'location' ცვლადი ჩავუწერე.
        return f.mkdir(); // 'mkdir()' მეთოდი 'File' კლასის ნაწილია. 'mkdir()' ფუნქცია გამოიყენება ახალი საქაღალდის შესაქმნელად.
    }

    // ამ ფუნქციის მეშვეობით შევქმენი ახალი '.txt' ფაილი კონკრეტულ ლოკაციაში, შემდეგ ჩავწერე კონკრეტული ტექსტი და დავხურე ფაილი.
    public void t1() { // // დავწერე ფუნქცია რომელსაც დავარქვი 't1'.
        try { // გამოვიყენე 'try' ოპერატორი.
            FileWriter writer = new FileWriter(location + "/data.txt"); // გამოვიძახე 'FileWriter' ოპერატორი, რომელშიც ჩავწერე ლოკაცია და ახალი ფაილის სახელი ამავე ფაილის შესაქმნელად.
            writer.write("29, 39, -90"); // 'varname.write()' მეშვეობით ზემოთ უკვე შექმნილ ფაილში ჩავწერე სასურველი ტექსტი.
            writer.close(); // დავხურე ფაილი.
        } catch (IOException e) {
            e.printStackTrace(); // 'printStackTrace()' მეთოდი 'Java'-ში არის ინსტრუმენტი, რომელიც გამოიყენება გამონაკლისებისა და შეცდომების დასამუშავებლად.
        }
    }

    // ამ ფუნქციაში შევქმენი კონკრეტული ფაილი რომელსაც დავარქვი 'data1.txt' და შევქმენი 'myFiles' საქაღალდეში.
    // შემდეგ მასში ჩავწერე მთელი რიცხვები 0-დან 100-ის ჩათვლით.
    public void t2() {
        try {
            FileWriter writer = new FileWriter(location + "/data1.txt");
            for (int i = 0; i <= 100; i++) {
                Integer wrt = i; // 'Integer' არის მთელი რიცხვები.
                writer.write(wrt.toString() + " "); // აქ ციფრები '.toString'-ის მეშვეობით გადავაქციე ჩვეულებრივ სიმბოლოებად.
            }
            writer.close(); // აქ დავხურე ფაილი.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ამ ფუნქციაში კლავიატურიდან გადავეცი ცვლადები, შემდეგ ამ ცვლადების შუალედიდან 'data2.txt' ფაილში ჩავწერე 100 შემთხვევითი რიცხვი.
    // ასევე ეს ფაილი შევქმენი 'myFiles' საქაღალდეში.
    public void t5() {
        // კლავიატურიდან გადავეცი [a; b] შუალედი.
        int a, b;
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();

        try {
            FileWriter writer = new FileWriter(location + "/data2.txt");
            for (int i = 0; i < 100; i++) {
                Integer wrt = (int) Math.floor(Math.random() * (b - a + 1) + a); // 'math.random' ვინაიდან ამოცანის პირობაში შემთხვევით რიცხვებზე იყო საუბარი.
                writer.write(wrt.toString() + ", ");
                wrt = null;
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ამ ფუნქციაში კონკრეტულ ფაილში დავამატე 50 შემთხვევითი ნამდვილი რიცხვი, დამატებული რიცხვები კი გამოვყავი მაგ ფაილში არსებული რიცხვებისგან.
    public void t6() {
        double a, b;
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();

        try {
            FileWriter writer = new FileWriter(location + "/data3.txt", true); // 'true' ამატებს.
            writer.write("\n=============\n");
            for (int i = 0; i <= 50; i++) {
                Integer wrt = (int) Math.floor(Math.random() * (b - a + 1) + a);
                writer.write(wrt.toString() + ", "); // '+'-ის შემდეგ ციფრები დავაშორე ერთმანეთს.
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ამ ფუნქციაში დავითვალე ფუნქციის მნიშვნელობები [0;2] შუალედში მეასედების სიზუსტით და შესაბამისი მნიშვნელობები ჩავწერე 'function.txt' ფაილში რომელიც მოთავსებულია 'myFiles' საქაღალდეში.
    public void t7() {
        try {
            FileWriter writer = new FileWriter(location + "/function.txt", true);
            writer.write("\nFormula: y= x^3 + e^x\n");
            StringBuilder sbldr = new StringBuilder();
            double x = Math.random() * 2;
            sbldr.append((Math.pow(x, 3) + Math.exp(x)));
            writer.write("x = " + x + "\ny = " + sbldr);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ამ ფუნქციაში 'myFiles' საქაღალდეში შევქმენი 10,000 '.txt' გაფართოების ფაილი, ფაილებში ჩავწერე ციფრები 1-დან 10,000მდე და ფაილის სახელი არის ის რაც მასში წერია. (მაგ, '2.txt' => 2).
    public void t9() {
        try {
            for (int i = 1; i <= 10000; i++) {
                FileWriter writer = new FileWriter(location + "/" + i + ".txt");
                Integer wrt = i;
                writer.write(wrt.toString() + " ");
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ამ ფუნქციის მეშვეობით კონკრეტული ათობითი რიცხვი გადავიყვანე ორობითში.
    public void t10(int number){
        System.out.println("ათობითი რიცხვი: "+number+" ორობითი რიცხვი: "+Integer.toBinaryString(number));
    }

    // ამ ფუნქციაში კი პირიქით, ორობითი რიცხვი გადავიყვანე ათობითში.
    public void t11(String bin){
        System.out.println("ორობითი რიცხვი: "+bin+" ათობითი რიცხვი: "+Integer.parseInt(bin, 2));
    }
}

