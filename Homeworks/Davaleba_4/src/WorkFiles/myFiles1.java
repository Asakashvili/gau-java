package WorkFiles;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class myFiles1 {
    // შემოვიტანე ცვლადი და მნიშვნელობად მივანიჭე კონკრეტული ლოკაცია ახალი ფოლდერის შესაქმნელად.
    protected String location = "src/MyFiles1";
    // 'protected'-ის მეშვეობით ეს ცვლადი ხელმისაწვდომი იქნება მხოლოდ ამ კლასში.

    // ამ ფუნქციის საშუალებით შევქმენი ახალი ფოლდერი.
    public boolean cFolder(){ // დავწერე ფუნქცია რომელსაც დავარქვი 'cFolder'.
        File f = new File(location); // გამოვიძახე 'File' ოპერატორი, ცვლადს დავარქვი 'f' და გავუტოლე შესაბამის მნიშვნელობას რასაც 'location' ცვლადი ჩავუწერე.
        return f.mkdir(); // 'mkdir()' მეთოდი 'File' კლასის ნაწილია. 'mkdir()' ფუნქცია გამოიყენება ახალი საქაღალდის შესაქმნელად.
    }

    // ამ ფუნქციის მეშვეობით 'myFiles1' საქაღალდეში შევქმენი 30 'data' '.txt' ფაილი, თითოეულს ჩავუწერე 'Programmer' ტექსტი და დავხურე ფაილი.
    public void t3(){
        try{
            for(int i=1; i<=30; i++){
                FileWriter writer = new FileWriter(location + "/data"+i+".txt");
                writer.write("Programmer");
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            }
        }
}

