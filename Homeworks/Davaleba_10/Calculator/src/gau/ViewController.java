package gau;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class ViewController {
    @FXML
    private TextField textField;

    @FXML
    private Text savedNumbers;

    private String firstNumber = "";

    private String currentNumber = "";

    private String calculationType;

    @FXML
    void addAction(ActionEvent event) {
        calculationSetup("+");
    }

    @FXML
    void minusAction(ActionEvent event) {
        calculationSetup("-");
    }

    @FXML
    void divideAction(ActionEvent event) {
        calculationSetup("/");
    }

    @FXML
    void multiplicationAction(ActionEvent event) {
        calculationSetup("*");
    }

    public void calculationSetup(String calculationType) {
        this.calculationType = calculationType;
        firstNumber = currentNumber;
        currentNumber = "";
    }

    @FXML
    void clearTextField(ActionEvent event) {
        currentNumber = ""; // განულება.
        textField.setText("0"); // ამის მეშვეობით უბრალოდ გამოგვაქვს შესაბამისი ტექსტი.
    }

    @FXML
    void button0(ActionEvent event) {
        if (!currentNumber.equals("")) {
            addNumber("0");
        }
    }

    @FXML
    void button1(ActionEvent event) {
        addNumber("1");
    }

    @FXML
    void button2(ActionEvent event) {
        addNumber("2");
    }

    @FXML
    void button3(ActionEvent event) {
        addNumber("3");
    }

    @FXML
    void button4(ActionEvent event) {
        addNumber("4");
    }

    @FXML
    void button5(ActionEvent event) {
        addNumber("5");
    }

    @FXML
    void button6(ActionEvent event) {
        addNumber("6");
    }

    @FXML
    void button7(ActionEvent event) {
        addNumber("7");
    }

    @FXML
    void button8(ActionEvent event) {
        addNumber("8");
    }

    @FXML
    void button9(ActionEvent event) {
        addNumber("9");
    }

    public void updateTextField() {
        textField.setText(currentNumber);
    }

    public void addNumber(String number) {
        currentNumber += number;
        updateTextField();
    }
}
