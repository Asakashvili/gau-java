package gau.Task_2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

// სერვერის მხარე.
public class Server_2 extends Thread {
    // განვსაზღვრე საჭირო ცვლადები.
    Socket socket;
    ObjectInputStream objectInputStream;
    String serverMessage, clientMessage;
    ObjectOutputStream objectOutputStream;
    // აქ გამოვიძახე მასივი.
    String[] RequestArr = {"Hmmm...", "Sorry could not get it.", "What?", "You can ask me these: help, date, name, error."};

    public void run(){
        try {
            ServerSocket serverSocket = new ServerSocket(8081); // სერვერი ელოდება. // გადავეცი ამოცანაში მითითებული პორტი.
            while (true) { // 'while'-ს მეშვეობით გაშვებული კოდი იქნება მუდმივი.
                serverMessage="----"; // ეს ავტომატურად ჩაიტვირთება ყოველი პასუხის გაცემის შემდგომ.
                // სერვერი იღებს მონაცემს.
                socket = serverSocket.accept();

                // მიღება -> წაკითხვა -> გამოტანა.
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                clientMessage = (String) objectInputStream.readObject();

                // აქ გამოვიტანე ქეისები, თუ რა მოხდეს სხვადასხვა მონაცემის დაწერის შემთხვევაში.
                switch (clientMessage.toLowerCase()){
                    case "bye", "goodbye", "exit":
                        System.exit(204);
                        break;
                    case "help":
                        File file = new File("src\\gau\\Task_2\\info.txt"); // აქედან შევქმენი ფაილი მითითებულ ლოკაციაზე.
                        if(file.createNewFile()){
                            System.out.println("create file info.txt;"); // რა გამოვიდეს ფაილის წარმატების შექმნის შემთხვევაში.
                        }else{
                            System.out.println("File info.txt already exists in the project root directory"); // რა გამოვიდეს იმ შემთხვევაში თუ ეს ფაილი უკვე არსებობს.
                        }
                        break;
                    case "date":
                        Date date = new Date();
                        System.out.println(date);
                        break;
                    case "name":
                        System.out.println("Giorgi");
                        break;
                    case "error":
                        System.out.println("Compile Error");
                        break;
                    case "thanks", "thanks!":
                        serverMessage = "You're welcome!";
                        break;
                    default:
                        serverMessage = RequestArr[(int)(Math.random()*RequestArr.length)];
                }
                // სერვერი აგზავნის მონაცემს.
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(serverMessage);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}