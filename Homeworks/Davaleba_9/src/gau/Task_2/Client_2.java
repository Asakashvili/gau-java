package gau.Task_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

// კლიენტის მხარე.
public class Client_2 extends Thread {
    // განვსაზღვრე საჭირო ცვლადები.
    Socket socket;
    ObjectOutputStream objectOutputStream;
    String serverMessage, clientMessage;
    ObjectInputStream objectInputStream;

    public void run(){
        try {
            Scanner sc = new Scanner(System.in); // აქ გამოვიძახე სკანერი.
            while(true){
                // კლიენტი აგზავნის მონაცემს.
                socket = new Socket(InetAddress.getByName("localhost"), 8081); // გამოვიძახე სოკეტი კლიენტსა და სერვერს შორის კომუნიკაციისთვის, ასევე ჩავუწერე '8081' პორტი რომელიც ამოცანაში იყო ნახსენები.
                System.out.print("Client: "); // დავბეჭდე 'Client: ' მომხმარებლის მიერ რაიმე კითხვის შესაყვანად.
                clientMessage = sc.nextLine(); // კითხვაზე პასუხის გაცემის შემდეგ ამის მეშვეობით გადავდივართ შემდეგ საფეხურზე.
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(clientMessage);

                // კლიენტი იღებს მონაცემს.
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                serverMessage = (String) objectInputStream.readObject();
                System.out.println("Server: " + serverMessage);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
