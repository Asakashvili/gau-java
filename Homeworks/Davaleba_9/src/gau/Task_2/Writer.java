package gau.Task_2;

import java.io.FileWriter;
import java.io.IOException;

public class Writer {
    public Writer(){
    try { // 'try' აუცილებელია 'FileWriter'-ისთვის.
        FileWriter fw = new FileWriter("src\\gau\\Task_2\\info.txt"); // გამოვიძახე "FileWriter" და მივუთითე კონკრეტული ფაილის ლოკაცია ამავე ფაილში სხვადასხვა რამის ჩასაწერად.
        // '.write'-ს მეშვეობით ფაილში ჩავწერე მონაცემები.
        fw.write("help\ncreate file info.txt;\n");
        fw.write("----\n");
        fw.write("date\nThu Jun 09 14:15:34 GET 2022\n");
        fw.write("----\n");
        fw.write("name\nGiorgi\n");
        fw.write("----\n");
        fw.write("Error\nCompile Error\n");
        fw.write("----\n");

        // აქ დავხურე 'FileWriter' - დავასრულე ჩაწერა.
        fw.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
    }
}
