package gau.Task_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

// სერვერის მხარე.
public class Server extends Thread{
    // განვსაზღვრე საჭირო ცვლადები.
    private Socket socket;
    private ObjectInputStream objectInputStream;
    private String message = "";
    private ObjectOutputStream objectOutputStream;

    public void run(){ // გამოვიყენე 'run()' მეთოდი ავტომატურად გასაშვებად.
        try {
            ServerSocket serverSocket = new ServerSocket(8080); // სერვერი ელოდება. // გადავეცი ამოცანაში მითითებული პორტი.
            while(true) { // 'while'-ს მეშვეობით გაშვებული კოდი იქნება მუდმივი.
                // სერვერი იღებს მონაცემს.
                socket = serverSocket.accept();

                // მიღება -> წაკითხვა -> გამოტანა.
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Server:-> " + message);

                // სერვერი აგზავნის მონაცემს.
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                Scanner sc = new Scanner(System.in);
                System.out.println("Server Enter: ");
                message = sc.nextLine();
                objectOutputStream.writeObject(message);

                // აქ დავამთავრე კოდი რომელიმე მხრიდან "goodbye"-ს დაწერის შემთხვევაში.
                if(message.equals("goodbye")){
                    System.exit(0);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
