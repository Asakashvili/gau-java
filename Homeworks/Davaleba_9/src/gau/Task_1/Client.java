package gau.Task_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

// კლიენტის მხარე.
public class Client extends Thread{
    // განვსაზღვრე საჭირო დახურული ცვლადები.
    private Socket socket;
    private ObjectOutputStream objectOutputStream;
    private String message = "";
    private ObjectInputStream objectInputStream;
    private String ip;

    public void run(){ // გამოვიყენე 'run()' მეთოდი ავტომატურად გასაშვებად.
        try {
            System.out.println("Enter IP Address: "); // აქ დავწერე პირველი მოთხოვნა 'while'-ს გარეთ რადგან ეს მოთხოვნა კოდის გაშვების შემდეგ ისევ არ გამოჩენილიყო.
            while(true){ // 'while'-ს მეშვეობით გაშვებული კოდი იქნება მუდმივი.
                // კლიენტი აგზავნის მონაცემს.
                socket = new Socket(InetAddress.getByName(ip), 8080); // აქ სოკეტში გადავეცი 'ip' ცვლადი და პორტი.
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                Scanner sc = new Scanner(System.in); // აქ უკვე გამოვიძახე სკანერი.
                ip = sc.nextLine();
                System.out.println("Client Enter: ");
                message = sc.nextLine();
                objectOutputStream.writeObject(message);

                // კლიენტი იღებს მონაცემს.
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Client:-> " + message);

                // აქ დავამთავრე კოდი რომელიმე მხრიდან "goodbye"-ს დაწერის შემთხვევაში.
                if(message.equals("goodbye")){
                    System.exit(0);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}