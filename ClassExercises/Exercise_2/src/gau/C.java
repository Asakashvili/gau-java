package gau;

import java.util.Random;
import java.util.Scanner;

public class C {
    int a, b;

    public C(){ // კონსქტრუქტორი.
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        System.out.println("a = ");
        a = sc.nextInt();
        System.out.println("b = ");
        b = sc.nextInt();

        // დაადგენს ლუწი და კენტი რიცხვების რაოდენობას შეტანილ 'a' და 'b' ცვლადების მნიშვნელობებს შორის.
        int odd = 0;
        int even = 0;
        for(int i=a; i<b; i++){

            // დავბეჭდე 'a' და 'b' შუაელდში შეტანილი რიცხვები.
            int rand1 = rand.nextInt(a, b);
            System.out.println(rand1);

            // შეტანილ რენდომ რიცხვებში გამოვყავი რომელია კენტი და რომელი ლუწი.
            if(rand1 % 2 == 0){
                odd++;
            }else if(rand1 % 2 == 1){
                even++;
            }else{
                System.out.println("დაფიქსირდა შეცდომა!");
            }
        }
        System.out.println("კენტი რიცხვების რაოდენობა: "+odd+".");
        System.out.println("ლუწი რიცხვების რაოდენობა: "+even+".");
    }
}
