package gau;

import java.util.Scanner;

public class B_inheritance extends B { // 'extend'-ის მეშვეობით მივანიჭე მემკვიდრეობა, ანუ 'B_inhertance' არის 'B'-ს შვილობილი.
    int y; // განვსაზღვრე მთელი ტიპის 'y' ცვლადი.

    // მივანიჭე 'x' და 'y' ცვლადებს მნიშვნელობები კლავიატურიდან.
    public void method1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("x = ");
        x = sc.nextInt();
        System.out.println("y = ");
        y = sc.nextInt();
    }

    // დავაბრუნებინე 'x' და 'y' ცვლადების ჯამი.
    public int method2(){
        return x+y;
    }
}
