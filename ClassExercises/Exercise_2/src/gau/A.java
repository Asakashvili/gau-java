package gau;

import java.util.Scanner;

public class A {
    int x, y;

    // ცვლადებს მნიშვნელობები მივანიჭე კლავიატურიდან.
    public void method1(){
        Scanner sc = new Scanner(System.in);
        System.out.println("x = ");
        x = sc.nextInt();
        System.out.println("y = ");
        y = sc.nextInt();
    }

    // დავაბრუნე პირველი ცვლადის მნიშვნელობის ბოლო ციფრი.
    public int method2(){
        return x%10;
    }

    // დავაბრუნე მეორე ცვლადის მნიშვნელობის პირველი ციფრი.
    public int method3(){
        int z = 0;
        while(y!=0){
            z = y%10;
            y = y/10;
        }
        return z;
    }
}

