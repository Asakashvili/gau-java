package gau;

import java.util.Scanner;

public class CalculatorV1 {
    double x, y, result; // 'double' - მცურავწერტილიანი რიცხვებისთვის.
    char z; // 'char' - საკვანძო სიტყვა გამოიყენება ერთი სიმბოლოს შესანახად. / 'char' - მოითხოვს მხოლოდ და მხოლოდ ერთ სიმბოლოს.
    // 'char' - მოითხოვს ნაკლებ მეხსიერებას ვიდრე 'String'. იგი ჯდება 'Stack'-ზე 'heap'-ის მაგივრად.

    public CalculatorV1(){
        // 'x', 'y' და 'z' ცვლადებს მივანიჭე მნიშვნელობები კლავიატურიდან.
        Scanner sc = new Scanner(System.in);

        // 'try-catch' ოპერატორები გამოიყენება Java-ში არასასურველი შეცდომების მოსაგვარებლად პროგრამის შესრულებისას.
        try { // 'try' ოპერატორი საშუალებას იძლევა განსაზღვროთ კოდის ბლოკი, რომელიც ამოწმებს შეცდომებს მისი შესრულებისას.
            System.out.println("შეიყვანეთ პირველი რიცხვი: ");
            x = sc.nextInt();
            System.out.println("აირჩიეთ ოპერატორი ('+' ; '-' ; '*' ; '/'): ");
            z = sc.next().charAt(0);
            System.out.println("შეიყვანეთ მეორე რიცხვი: ");
            y = sc.nextInt();

            if (z == '/') {
                result = x / y;
            } else if (z == '+') {
                result = x + y;
            } else if (z == '-') {
                result = x - y;
            } else if (z == '*') {
                result = x * y;
            } else {
                // 'throw' საკვანძო სიტყვა Java-ში გამოიყენება გამონაკლისის გამოსათვლელად მეთოდიდან ან ნებისმიერი კოდის ბლოკიდან.
                throw new Exception("დაფიქსირდა შეცდომა!"); // 'throw new Exception'-ის შემოტანის შემთხვევაში აღარ დაიბეჭდება 0.00.
            }
            System.out.println("პასუხი: "+result);

            // Your 'catch' block will literally 'catch' an exception object that was 'thrown' at some point-
            // -during a 'try' block and store it in the 'e' variable (a.k.a parameter).
        }catch(Exception e){ // 'catch' - ოპერატორი საშუალებას იძლევა განსაზღვროთ შესასრულებელი კოდის ბლოკი, თუ შეცდომა მოხდა try ბლოკში.
            System.out.println(e.getMessage()); // 'e' არის უბრალო ცვლადი / 'Exception'-ის სახელი;
            // 'getMessage()' მეთოდი გამოიყენება 'Throwable' ობიექტის დეტალური შეტყობინების დასაბრუნებლად, რომელიც ასევე შეიძლება იყოს null.
        }
    }
}
