package gau;

public class Main {

    public static void main(String[] args) {

        // ორი გზა "String"-ის დასაბეჭდად რომელსაც ერთიდაიგივე შედეგი გამოაქვს.
        String s1 = "Java"; // გამოვიძახე "String" ოპერატორი, მივანიჭე ცვლადი რომელსაც დავარქვი "s1" და გავუტოლე კონკრეტულ მნიშვნელობას.
        String s2 = new String("Java");
        System.out.println(s1); // "System.out.println" მეშვეობით დავბეჭდე ზემოთ მოცემული ცვლადის მნიშვნელობა ცვლადის გადაცემით.
        System.out.println(s2);

        // =======================================================================

        // მასივის სიმბოლოებისგან ერთი მთლიანი სტრიქონის ჩამოყალიბება.
        // დავწერე მასივი რომელსაც დავარქვი 'a' და შემდეგ მასივს ჩავუწერე ელემენტები.
        char[] a = {'j', 'a', 'v', 'a'}; // 'char' საკვანძო სიტყვა არის მონაცემთა ტიპი, რომელიც გამოიყენება ერთი სიმბოლოს შესანახად.
        String b = new String(a);
        System.out.println(a);

        // =======================================================================

        // Length(), charAt() და indexOf() მეთოდების გამოყენება.
        String str1 = new String("Java tutorial");
        System.out.println("The character at index 6 is : " + str1.charAt(6));
        System.out.println("The length of the input string is : " + str1.length());
        System.out.println("The index of letter 'v' is : " + str1.indexOf('v'));

        // =======================================================================

        // Compare(), contentequals(), და includes() მეთოდების გამოყენება.
        String s3 = new String("Java tutorial");
        System.out.println("Comparison of input string with argument is : " + s3.compareTo("C++"));
        System.out.println("Output of contains method: " + s3.contains("tutorial"));

        // =======================================================================

        // სტრიქონების ერთმანეთთან მიერთება.
        String s4 = "java tutorial";
        String s5 = "Java Tutorial";
        String s6 = "java tutorial";
        String s7 = "Tutorial cup";
        System.out.println(s1.equals(s2));
        System.out.println(s1.equals(s3));
        System.out.println(s1.equals(s4));

        // =======================================================================

        // სტრიქონის სიმბოლოების გადიდება და დაპატარავება toUpperCase() და toLowerCase() მეთოდებით.
        String s8 = "Welcome to tutorialcup";
        System.out.println("Convert to lower case: " + s1.toLowerCase());
        System.out.println("Convert to upper case: " + s1.toUpperCase());

        // =======================================================================

        // ჯავაში საბსტრინგის გამოყენება.
        String s9 = "java tutorial";
        System.out.println(s1.substring(3));
        System.out.println(s1.substring(1, 10));
            }
        }


