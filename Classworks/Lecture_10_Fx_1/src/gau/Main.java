package gau;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
	// write your code here
        launch(); // ქვემოთ სტარტის გასაშვებად საჭიროა 'Main'-ში 'launch()' ფუნქციის დაწერა.
    }

    // ეს გამოვა 'Main'-ზე 'Application'-ის მიწერის შემდგომ.
    @Override
    public void start(Stage stage) throws Exception {
        // 'view.fxml' ჩატვირთვის/გამოჩენის პროცესი.
        // 'Parent' ელემენტის მეშვეობით შემოგვაქვს 'AnchorPane' (განლაგება) და გადავეცით თუ საიდან შემოგვაქვს.
        Parent parent = FXMLLoader.load(getClass().getResource("view.fxml"));
        Scene scene = new Scene(parent);
        stage.setScene(scene);

        // სანამ 'stage'-ს გამოვაჩენთ შეგვიძლია მასზე რაიმე მანიპულაციები ჩავატაროთ.
        stage.setTitle("Java Fx Application!!!"); // ეს არის აპლიკაციის 'Title'.

        stage.show();
    }
}
