package gau;

public class Person {
    int x;
    double y;

    public Person(int x){
        this.x = x;
    }

    public Person(int x, double y){ // 'Generate' - საშუალებით.
        this.x = x; // 'this' მიანიშნებს კონკრეტულ ობიექტზე.
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x){
        this.x = x;
    }

    public Person(){ // კონსტრუქტორში, დასაბრუნებელ ტიპებში არაფერი არ იწერება. (მხოლოდ 'public') მაგ: 'void'.
        // ცარიელი კონსტრუქტორი.
        System.out.println("Constructor___");
    }

    public Person(int x, int y){ // პარამეტრიანი კონსქტრუქტორი / აუცილებლად უნდა გვქონდეს უპარამეტროც. ^
        System.out.println(x+y);
    }

    public Person(PersonalInformation o){ // ჩავუწერეთ/გადავეცით კლასი.

    }
}
