package gau;

import java.util.Scanner;

// ფაილის და კლასის სახელები უნდა იყოს ერთიდაიგივე, ერთი კლასი ერთი ფაილის შიგნით.
public class PersonalInformation { // კლასი.
    // კლასის შიგნით არსებული ობიექტები, ატრიბუტები.
    int  x; // ცვლადი.
    int m []; // მასივი.
    Scanner scanner; // სკანერი.
    int k = 0;

    int z = 89;

    void method6(int z){
        System.out.println(z);
        System.out.println(this.z); // 'z' ცვლადზე გვაქვს წვდომა.
    }

    void method1(){ // მეთოდი, იგივე რაც ფუნქცია, წინ ვუწერთ 'void'-ს ანუ არაფერს არ დააბრუნებს და არც პარამეტრები არ აქვთ.
        System.out.println("Hello"); // უბრალოდ 'Hello'-ს დაბეჭდავს და არც არაფერს დააბრუნებს და არც პარამეტრი არ აქვს.
    }
    void method2(int x, double y, String s){ // არაფერს არ აბრუნებს მაგრამ პარამეტრები შეიძლება რომ ჰქონდეს.
        System.out.println("x = "+x+" Y= "+y);
    }
    int method3(){ // 'int' ანუ ეს მეთოდი რაღაცას გვიბრუნებს მაგრამ პარამეტრები არააქვს.
        if(true){
           x = 4; // ცვლადი 'X' ზემოთ განვსაზღვრეთ.
        }else{
            x = 9;
        }
        return x; // 'return'-ის დაწერა ბოლოშია მიღებული. მეთოდში ბევრი 'return'-ის დაწერა არ შეიძლება.
    }

    int method4(int y, int z){
        return y+z;
    }

    int method5(){

        return k;
    }
}

