package gau;

// აქ არის შემოტანილი იტერატორი.
import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        List a = new ArrayList();
        a.add(34);
        a.add("Hello");
        List b = new LinkedList();
        ArrayList a1 = new ArrayList(Arrays.asList(1, 2, 3.5, 4, 5, 1));
        List<Integer> list = new ArrayList<>();
        System.out.println(a1);
        System.out.println(a1.get(3));
        for(int i=0; i<a1.size(); i++){
            System.out.println(a1.get(i));
        }


        for(Object el:a1){
            System.out.println(el);
        }
        ListIterator iterator = a1.listIterator();
        System.out.println("==========");
        // ამოწმებს შემდეგი არის თუ არა.
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        Hashtable <String, Integer> d1 = new Hashtable();
        // მონაცემების ჩაწერა.
        d1.put("first", 45);

        Set s = new HashSet();
        s.add(2);
        s.add(2);
        s.add(4);

        // იქნება ორ ელემენტიანი რადგან ერთნაირ ელემენტებს არ სვავს, (2, 2)-ს.
        System.out.println(s);

        Iterator iterator1 = s.iterator();
        System.out.println("==========");
        Queue q1 = new PriorityQueue();
        q1.add(92);
        q1.add(3);
        q1.add(98);
        q1.add(87);
        q1.add(9);
//        System.out.println(q1);
        q1.add(70);
//        System.out.println(q1);
//        while(q1.size() != 0) {
//            System.out.println(q1.poll());
//        }
        System.out.println("===");
        for(Object el:q1){
            System.out.println(el);
        }

        System.out.println("==========");
        Queue q2 = new ArrayDeque();
        q2.add(3);
        q2.add(1);
        q2.add(34);
        System.out.println(q2);
    }
}
