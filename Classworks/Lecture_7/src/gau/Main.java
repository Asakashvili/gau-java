package gau;

import thread.TestThread1;
import thread.TestThread2;

public class Main {

    public static void main(String[] args) {
	// write your code here
//        Thread.currentThread().setPriority(9); // 'main'-ს მივეცით პრიორიტეტი, მიმდინარე ნაკადი ყოველთვის არის 'main'.
        TestThread1 t1 = new TestThread1();
//        t1.setPriority(9); // პრიორიტეტი გავუზარდეთ 9-მდე.
        t1.start(); // 'start'-ით ნაკადი გადავა 'running' მდგომარეობაში.

        TestThread2 t2 = new TestThread2();
        Thread _t2 = new Thread(t2); // კონსტრუქტორში გადავეცი 't2' ობიექტი.
        _t2.start();

//        t1 = null;
//        t1.stop(); // ვაჩერებთ ნაკადს.
//        System.out.println("Th 0-" + t1.getPriority());
//        t1.printClassName();
//        System.out.println(t1.getName());

//        System.out.println(Thread.currentThread().getName());
//        System.out.println(Thread.currentThread().getPriority());
//
//        System.out.println("End of Main");

        for(int i=0; i<10; i++){
            System.out.println("Main - "+i);
        }
    }
}
