package thread;

public class TestThread1 extends Thread{
    public void run(){
        printNumbers(); // გამოვიძახე მეთოდი.
    }
    public void printClassName(){
        System.out.println("TestThread1");
    }
    public void printNumbers(){
        for(int i=0; i<10; i++){
//            try {
//                Thread.sleep(10); // ნაკადი დავაყოვნეთ 10 მილიწამით.
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            System.out.println(this.getName()+" - "+i);
        }
    }
}
