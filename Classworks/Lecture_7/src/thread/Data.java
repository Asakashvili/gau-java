package thread;

public class Data {
    public int x = 1000;

    public synchronized void method(){ // 'synchronized' მეთოდი ნაკადებს ერთნაირად აღარ გამოიძახებს. (სანამ პირველი არ დასრულდება).
        for(int i=0; i<10; i++)
            System.out.println(Thread.currentThread().getName()+" - "+i);
        }
    }
