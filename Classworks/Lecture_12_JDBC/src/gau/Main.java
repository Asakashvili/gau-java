package gau;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        // ბაზასთან დაკავშირება.
        String url = "jdbc:mysql://localhost:3306/gaujava2022";
        try {
            Connection conn = DriverManager.getConnection(url, "root", "");

//            // ჩაწერა.
//            Statement st = conn.createStatement();
//            st.executeUpdate("INSERT INTO users(name) VALUE ('ვალერი')");

            // წაშლა - 'Prepared Statement'.
            PreparedStatement pr = conn.prepareStatement("DELETE FROM users WHERE id<?");
            pr.setInt(1, 2);
            pr.executeUpdate();

            // მოთხოვნების დამუშავება / წამოღება.
//            Statement st = conn.createStatement();
//            ResultSet rs = st.executeQuery("SELECT * FROM users ");
//            rs.next(); // საჭიროა შემდეგ ჩანაწერზე გადასასვლელად.
//            System.out.println(rs.getString("name"));
//            // 'while'-ში გატარების შემთხვევაში ყველა ჩანაწერს წამოიღებს!
//            while(rs.next()){
//                System.out.println(rs.getString("name"));
//            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
