package files;

import java.io.*;

public class FilesAndFolder {
    public void toWrite(String t){ // 'FileWriter'-თან მუშაობის დროს აუცილებელია 'try & catch', 'throw' არგამოდგება.
        try {
            // ამის მეშვეობით შეიქმნა '.txt' ფაილი.
            FileWriter writer = new FileWriter("test.txt", true); // 'boolean true' გადაწერის მაგივრად დაამატებს.
            writer.write(t);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace(); // 'printStackTrace()' მეთოდი 'Java'-ში არის ინსტრუმენტი, რომელიც გამოიყენება გამონაკლისებისა და შეცდომების დასამუშავებლად.
        }
    }


    public void read(){
        try {
            BufferedReader rd = new BufferedReader(new FileReader("test.txt")); // კონსტრუქტორში გადაეცემა 'FileReader', ის ფაილს ხსნის და კითხულობს კონსტრუქტორში.
//            System.out.println(rd.readLine()); // 'readLine'-ს რამდენჯერაც დავწერთ იმდენ სტრიქონს წაიკითხავს.
//            System.out.println(rd.readLine());
            String s;
            while ((s=rd.readLine())!=null){ // სანამ 'null'-ს არ დაგვიბრუნებს მანამდე ანიჭოს.
                System.out.println(s); // საბოლოოდ დაიბეჭდება ყველა სტრიქონი.
            }
            rd.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace(); // 'printStackTrace()' მეთოდი 'Java'-ში არის ინსტრუმენტი, რომელიც გამოიყენება გამონაკლისებისა და შეცდომების დასამუშავებლად.
        }
    }
}
