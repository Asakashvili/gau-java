package gau;

import test1.Test1;

// შვილობილში ხილვადია 'public' და 'protected'.
public class Test4 extends Test1 { // 'Test1'-ის შვილობილი.
    public void print(){
        System.out.println(this.x); // 'this.' გვაჩვენებს რაარის ხელმისაწვდომი, სხვა შემთხვევაში მხოლოდ ცვლადის სახელის ჩაწერაც შეგვიძლია.
        System.out.println(this.z);
    }
}
