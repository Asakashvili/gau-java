package gau; // 'package' - საქაღალდე / გამოიყენება პროგრამული სტრუქტურის მარტივად გადანაწილებისთვის.

import files.FilesAndFolder;
import test1.Test1; // დაგვიიმპორტნა 'package'.

public class Main {

    public static void main(String[] args) {
	// write your code here

//        // იმპორტით.
//        Test1 t1 = new Test1();
//        // თუ იმპორტი არგვინდა ვუთითებთ ლოკაციას.
//        test2.Test1 t2 = new test2.Test1();
//        Test3 t3 = new Test3();
//        System.out.println(t3.x); // დავბეჭდეთ 't3'-ის 'x' ცვლადის მნიშვნელობა. - 'public'-ზე გვაქვს წვდომა.
//        System.out.println(t3.y); // დავბეჭდეთ 't3'-ის 'x' ცვლადის მნიშვნელობა. - არაფერი როარააქვს მაინც გვაქვს წვდომა.
//        System.out.println(t3.z); // დავბეჭდეთ 't3'-ის 'x' ცვლადის მნიშვნელობა. - დაცულზეც მაინც გვაქვს წვდომა მხოლოდ 'package'-ს ფარგლებში..
////        System.out.println(t3.a); // 'private' არაა სხვა კალსებში ხელმისაწვდომი.
//
//        System.out.println("===============");
//        Test1 t1 = new Test1();
//        System.out.println(t1.x); // მხოლოდ 'public' არის სხვა 'package'-ში ხელმისაწვდომი.
//        System.out.println("===============");
//        Test4 t4 = new Test4();
//        t4.print();

        FilesAndFolder f = new FilesAndFolder();
//        f.toWrite("\nC++"); // ამის მეშვეობით შეიქმნა '.txt' ფაილი.
        f.read();
    }
}
