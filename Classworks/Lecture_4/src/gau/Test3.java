package gau;

public class Test3 {
    // 'public, int და protected' ერთნაირად მუშაობენ.
    public int x = 10;
    int y = 11;
    protected int z = 12; // 'protected' დაცულია და ხელმისაწვდომია ყველგან ერთი 'package'-ს ფარგლებში.

    // 'private' მხოლოდ ამ კლასში გამოიყენება.
    private int a = 13;
}
