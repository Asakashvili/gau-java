package gau;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application { // ვაქციეთ 'extends'-ად, მივუწერეთ აპლიკაცია.

    public static void main(String[] args) {
	// write your code here
        launch(); // ქვემოთ სტარტის გასაშვებად საჭიროა 'Main'-ში 'launch()' ფუნქციის დაწერა.
    }

    @Override
    public void start(Stage stage) throws Exception { // გავაკეთეთ მეთოდის იმპლემენტაცია 'extends'-დან.
        Parent parent = FXMLLoader.load(getClass().getResource("view.fxml"));
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();
    }
}
