package gau;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.Random;

// აქედანაც შეგვიძლია 'view.fxml' ფაილში სხვადასხვა კომპონენტების დამატება.

public class ViewController {
    @FXML // 'Controller'-ში შემოგვაქვს ფლაგები.
    private AnchorPane main; // იდენფიცირების მოხდენა.

    public void add(){ // რადგანაც 'view.fxml' ფაილში კონკრეტულ ღილაკს 'onAction'-ში ჩავუწერეთ 'add', აქ კლასს გადავეცით იგივე სახელი კონკრეტულ ღილაკზე სხვადასხვა მანიპულაციების მოსახდენად.
//        Platform.exit(); // ეს გამოიყენება აპლიკაციის დახურვისთვის. (File->Close).

        // წაიშლება მთლიანი მენიუ (AnchorPoint);
//        main.getChildren().clear(); // ამის მეშვეობით აპლიკაციაში 'Close' ღილაკზე დაჭერის შემდეგ ეგ ღილაკები საერთოდ გაქრება და დარჩება მხოლოდ და მხოლოდ მიღებული შედეგი.

        VBox vBox = new VBox(); // ვერტიკალური ბოქსი.
        // აქედან 'b1' ღილაკს მივანიჭე შესაბამისი პოზიცია.
        vBox.setLayoutY(60);
        vBox.setLayoutX(30);
        vBox.getChildren().clear(); // ამის მეშვეობით 'Close'-ზე ყოველი დაწკაპების შემდეგ ღილაკები შეიცვლება. // ამას თუ არ გავაკეთებთ ზეგ გადააწერს.

        // '4x5' ღილაკები შემთხვევითი ციფრებით.
        for(int i=0; i<5; i++) { // ვერტიკალურად გამოვა
            HBox hBox = new HBox(); // ჰორიზონტალური ბოქსი.
            hBox.setSpacing(10); // 'h(orizontal)Box.setSpacing' ჰორიზონტალურად აშორებს ღილაკებს ერთმანეთისგან.
            vBox.getChildren().add(hBox);
            Random random = new Random();
            for(int j=0; j<4; j++) { // ჰორიზონტალურად გამოვა 4 სვეტი.
                String rand = String.valueOf(random.nextInt(-2, 2)); // ღილაკებში ჩიფრები ჩაიწერება შემთხვევითად (-2, -1, 0, 1, 2);
                // აპლიკაციაში თავდაპირველად 'Close' ღილაკზე დაჭერის შემდეგ ზედა მარცხნივ კუთხეში დაემატება 'b1' ღილაკი.
                Button b1 = new Button(rand); // 'b1' ღილაკს გადავეცით მნიშვნელობა.
                b1.setPrefWidth(30); // ამის მეშვეობით ყველა ღილაკს მივანიჭეთ ერთი და იმავე ზომა (სიგანე).
                hBox.getChildren().add(b1);
            }
        }
        vBox.setSpacing(10); // 'v(ertical)Box.setSpacing' ვერტიკალურად აშორებს ღილაკებს ერთმანეთისგან.
        main.getChildren().add(vBox);
    }
}
