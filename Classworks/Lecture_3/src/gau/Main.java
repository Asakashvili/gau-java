package gau;

public class Main {

    public static void main(String[] args) {
	// write your code here

        int []m = {3, 4, 5};
        int x = 0;
        int y = 0;
        try { // 'Try'-ში ვწერთ საეჭვოდ განსაზღვრულ კოდს.
            x = 3;
            x /= 0;
            x += m[3];
            x += 2;
            y = 8;
//            System.out.println(m[3]); // თუ დაფიქსირდა რაიმე გამონაკლისი შემთხვევა
            // 'ArithmeticException' - არის შეცდომა, რომელიც დაშვებულია არასწორი არითმეტიკული სიტუაციის დროს.
        }catch(ArithmeticException e){ // 'Catch'-ში ვწერთ იმ ტიპის პარამეტრს რომელიც წინასწარ ვიცით რომ ეს შეიძლება დაფიქსირდეს.
            x += 10; // დაამატოს კიდევ 10.
            System.out.println(e.getMessage());
            System.out.println(e.getClass());
            y = 9;
        }finally{
//            System.out.println(y);
            y += 2; // გავზარდე 2-ით.
            System.out.println("Finally");
        }
        y += 3;
//        catch (ArithmeticException e){
//            x += 20;
//        }
        System.out.println(y);
        System.out.println(x+2);
    }
}
