package gau;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

// 'Client'-მა და 'Server'-მა ერთდროულად უნდა იმუშაონ, რადგან სხვაგვარად ერთმანეთს ვერ დაუკავშირდებიან.
public class Client extends Thread {
    private Socket socket;
    private ObjectOutputStream objectOutputStream;
    private String message = ""; // ცარიელ ცვლადს რომ ვსაზღვრავთ ჯობს რომ რაიმე ჩაიწეროს ხოლმე შიგნით.
    private ObjectInputStream objectInputStream;


    public void run(){ // 'run' მეთოდი ავტომატურად ეშვება.
        try { // მოითხოვა 'try' & 'catch'.
            while (true) { // ჩავსვით 'while'-ში რომ მუდმივი ყოფილიყო.
                // 'socket'-ით დავამყარეთ რაღაც კავშირი.
                // კლიენტი აგზავნის.
                socket = new Socket(InetAddress.getByName("localhost"), 8888); // გადაეცა 'localhost' თავისი პორტით.
                // ამ კავშირით 'object'-ში რაიმეს ჩაწერა.
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in);
                System.out.println("Client Enter: ");
                message = scanner.nextLine();
                objectOutputStream.writeObject(message);

                // კლიენტი იღებს მონაცემს.
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Client:-> " + message);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
