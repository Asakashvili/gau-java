package gau;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server extends Thread{
    private Socket socket;
    private ObjectInputStream objectInputStream;
    private String message = "";
    private ObjectOutputStream objectOutputStream;

    public void run(){
        try {
            // სერვერი ელოდება.
            ServerSocket serverSocket = new ServerSocket(8888);
//            System.out.println("Server::: ");
//            Scanner scanner1 = new Scanner(System.in);
//            String message1 = scanner1.nextLine();

            while(true){
                // სერვერი იღებს მონაცემს
                socket = serverSocket.accept();
                // მიღება -> წაკითხვა -> გამოტანა.
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Server:-> " + message);

                // სერვერი აგზავნის.
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in);
                System.out.println("Server Enter: ");
                message = scanner.nextLine();
                objectOutputStream.writeObject(message);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
