package gau;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Client client = new Client();
        Server server = new Server();

        // ორივე დავსტარტეთ რომ ცალ-ცალკე ნაკადებში იყოს.
        client.start();
        server.start();
    }
}
